import { c as useStorage, _ as __nuxt_component_0 } from "./Breadcrumb-DTrAn1yb.js";
import { _ as __nuxt_component_1 } from "./client-only-CF-yOZJ2.js";
import { g as useToast, f as useRouter, d as __nuxt_component_4 } from "../server.mjs";
import { defineComponent, ref, mergeProps, withCtx, createVNode, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import "tailwind-merge";
import "hookable";
import "#internal/nitro";
import "ofetch";
import "unctx";
import "h3";
import "unhead";
import "@unhead/shared";
import "vue-router";
import "ufo";
import "defu";
import "klona";
import "devalue";
import "@iconify/vue/dist/offline";
import "@iconify/vue";
import "ohash";
const _sfc_main = /* @__PURE__ */ defineComponent({
  __name: "new-project-template",
  __ssrInlineRender: true,
  setup(__props) {
    const toast = useToast();
    const router = useRouter();
    async function checkAndpursuit() {
      if (this.data.project.template === "") {
        toast.add({
          id: "error",
          title: "Erreur",
          description: "clique sur une carte pour continuer",
          timeout: 8e3
        });
      } else {
        router.push("/new-project-message");
      }
    }
    const links = [{
      label: "Home",
      icon: "i-heroicons-home",
      to: "/"
    }, {
      label: "Template",
      icon: "i-heroicons-square-3-stack-3d"
    }];
    ref(false);
    const project = { project: { name: "", template: "", message: "", messageTitle: "" } };
    useStorage("data", project);
    return (_ctx, _push, _parent, _attrs) => {
      const _component_UBreadcrumb = __nuxt_component_0;
      const _component_ClientOnly = __nuxt_component_1;
      const _component_UButton = __nuxt_component_4;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "space-y-8" }, _attrs))}>`);
      _push(ssrRenderComponent(_component_UBreadcrumb, { links }, null, _parent));
      _push(`<h1>Choisis ta carte</h1>`);
      _push(ssrRenderComponent(_component_ClientOnly, null, {
        fallback: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`<div class="bg-gradient-to-tr to-slate-500 from-slate-300 animate-pulse h-[450px] w-10/12 sm:w-2/3 mx-auto p-2 rounded border"${_scopeId}></div><div class="bg-gradient-to-tr to-slate-500 from-slate-300 animate-pulse h-[450px] w-10/12 sm:w-2/3 mx-auto p-2 rounded border"${_scopeId}></div><div class="bg-gradient-to-tr to-slate-500 from-slate-300 animate-pulse h-[450px] w-10/12 sm:w-2/3 mx-auto p-2 rounded border"${_scopeId}></div><div class="bg-gradient-to-tr to-slate-500 from-slate-300 animate-pulse h-[450px] w-10/12 sm:w-2/3 mx-auto p-2 rounded border"${_scopeId}></div>`);
          } else {
            return [
              createVNode("div", { class: "bg-gradient-to-tr to-slate-500 from-slate-300 animate-pulse h-[450px] w-10/12 sm:w-2/3 mx-auto p-2 rounded border" }),
              createVNode("div", { class: "bg-gradient-to-tr to-slate-500 from-slate-300 animate-pulse h-[450px] w-10/12 sm:w-2/3 mx-auto p-2 rounded border" }),
              createVNode("div", { class: "bg-gradient-to-tr to-slate-500 from-slate-300 animate-pulse h-[450px] w-10/12 sm:w-2/3 mx-auto p-2 rounded border" }),
              createVNode("div", { class: "bg-gradient-to-tr to-slate-500 from-slate-300 animate-pulse h-[450px] w-10/12 sm:w-2/3 mx-auto p-2 rounded border" })
            ];
          }
        })
      }, _parent));
      _push(ssrRenderComponent(_component_UButton, {
        label: "Suivant",
        onClick: ($event) => checkAndpursuit()
      }, null, _parent));
      _push(`</div>`);
    };
  }
});
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/new-project-template.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as default
};
//# sourceMappingURL=new-project-template-CLZ1xG9T.js.map
