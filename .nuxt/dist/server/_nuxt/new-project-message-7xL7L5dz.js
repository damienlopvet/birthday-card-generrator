import { c as useStorage, _ as __nuxt_component_0 } from "./Breadcrumb-DTrAn1yb.js";
import { _ as __nuxt_component_1 } from "./client-only-CF-yOZJ2.js";
import { f as useRouter, g as useToast, d as __nuxt_component_4 } from "../server.mjs";
import { ref, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import "tailwind-merge";
import "hookable";
import "#internal/nitro";
import "ofetch";
import "unctx";
import "h3";
import "unhead";
import "@unhead/shared";
import "vue-router";
import "ufo";
import "defu";
import "klona";
import "devalue";
import "@iconify/vue/dist/offline";
import "@iconify/vue";
import "ohash";
const _sfc_main = {
  __name: "new-project-message",
  __ssrInlineRender: true,
  setup(__props) {
    useRouter();
    useToast();
    ref(false);
    const project = { project: { name: "", template: "", message: "", messageTitle: "", messageAuthor: "" } };
    useStorage("data", project);
    const links = [{
      label: "Home",
      icon: "i-heroicons-home",
      to: "/"
    }, {
      label: "Template",
      icon: "i-heroicons-square-3-stack-3d"
    }, {
      label: "Messsage",
      icon: "i-heroicons-chat-bubble-left-ellipsis-solid"
    }];
    return (_ctx, _push, _parent, _attrs) => {
      const _component_UBreadcrumb = __nuxt_component_0;
      const _component_ClientOnly = __nuxt_component_1;
      const _component_UButton = __nuxt_component_4;
      _push(`<form${ssrRenderAttrs(mergeProps({ class: "space-y-8" }, _attrs))}>`);
      _push(ssrRenderComponent(_component_UBreadcrumb, { links }, null, _parent));
      _push(`<h1>Ecris ton message</h1><hr><h2 class="text-red-300"><i>Nous écrirons les prénoms de tes invités à la prochaine étape</i></h2>`);
      _push(ssrRenderComponent(_component_ClientOnly, null, {}, _parent));
      _push(ssrRenderComponent(_component_UButton, {
        type: "submit",
        label: "Suivant"
      }, null, _parent));
      _push(`</form>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/new-project-message.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as default
};
//# sourceMappingURL=new-project-message-7xL7L5dz.js.map
