const interopDefault = r => r.default || r || [];
const styles = {
  "node_modules/nuxt/dist/app/entry.js": () => import('./entry-styles.BQ70eBJv.mjs').then(interopDefault),
  "app.vue": () => import('./app-styles.DcZs23V-.mjs').then(interopDefault),
  "app.vue?vue&type=style&index=0&lang.css": () => import('./app-styles.DcZs23V-.mjs').then(interopDefault),
  "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue": () => import('./error-404-styles.D3jhguou.mjs').then(interopDefault),
  "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue": () => import('./error-500-styles.BNqVly3Z.mjs').then(interopDefault),
  "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue?vue&type=style&index=0&scoped=ccd3db62&lang.css": () => import('./error-404-styles.D3jhguou.mjs').then(interopDefault),
  "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue?vue&type=style&index=0&scoped=df79c84d&lang.css": () => import('./error-500-styles.BNqVly3Z.mjs').then(interopDefault),
  "node_modules/@nuxt/ui/dist/runtime/components/elements/Carousel.vue": () => import('./Carousel-styles.B3gzFa9R.mjs').then(interopDefault),
  "node_modules/@nuxt/ui/dist/runtime/components/elements/Carousel.vue?vue&type=style&index=0&scoped=cb7e1dab&lang.css": () => import('./Carousel-styles.B3gzFa9R.mjs').then(interopDefault),
  "node_modules/nuxt-icon/dist/runtime/IconCSS.vue": () => import('./IconCSS-styles.D5ci0eBM.mjs').then(interopDefault),
  "node_modules/nuxt-icon/dist/runtime/IconCSS.vue?vue&type=style&index=0&scoped=41e8d397&lang.css": () => import('./IconCSS-styles.D5ci0eBM.mjs').then(interopDefault),
  "node_modules/nuxt-icon/dist/runtime/Icon.vue": () => import('./Icon-styles.C4x5-3rq.mjs').then(interopDefault),
  "node_modules/nuxt-icon/dist/runtime/Icon.vue?vue&type=style&index=0&scoped=46c2e1f1&lang.css": () => import('./Icon-styles.C4x5-3rq.mjs').then(interopDefault)
};

export { styles as default };
//# sourceMappingURL=styles.mjs.map
