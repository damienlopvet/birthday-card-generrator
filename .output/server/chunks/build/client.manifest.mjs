const client_manifest = {
  "_BirthdayCard.BWQLCvui.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "BirthdayCard.BWQLCvui.js",
    "imports": [
      "_Divider.DWWHL2Rb.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_Breadcrumb.hG4ohCDR.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Breadcrumb.hG4ohCDR.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_ButtonGroup.ub5GH_tG.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "ButtonGroup.ub5GH_tG.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_Divider.DWWHL2Rb.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Divider.DWWHL2Rb.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_Input.B69YdhzR.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "Input.B69YdhzR.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_client-only.BiGpKdjH.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "client-only.BiGpKdjH.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "layouts/default.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "default.9cf-vrJw.js",
    "src": "layouts/default.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "error-404.o-kvvLev.js",
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "css": []
  },
  "error-404.BOwFbGAB.css": {
    "file": "error-404.BOwFbGAB.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "error-500.-x3XxSJ_.js",
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "css": []
  },
  "error-500.CzZUE1u9.css": {
    "file": "error-500.CzZUE1u9.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/nuxt-icon/dist/runtime/IconCSS.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "IconCSS.Cul5NZgi.js",
    "src": "node_modules/nuxt-icon/dist/runtime/IconCSS.vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "css": []
  },
  "IconCSS.Z2BAHt_z.css": {
    "file": "IconCSS.Z2BAHt_z.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/nuxt/dist/app/entry.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "entry.DHvIcb5Q.js",
    "src": "node_modules/nuxt/dist/app/entry.js",
    "isEntry": true,
    "dynamicImports": [
      "layouts/default.vue",
      "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue",
      "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue"
    ],
    "css": [
      "entry.C7HF73WI.css"
    ],
    "_globalCSS": true
  },
  "entry.C7HF73WI.css": {
    "file": "entry.C7HF73WI.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "pages/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.CxphukV-.js",
    "src": "pages/index.vue",
    "isDynamicEntry": true,
    "imports": [
      "_Breadcrumb.hG4ohCDR.js",
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "css": [
      "index.CyC-1HNi.css"
    ]
  },
  "index.CyC-1HNi.css": {
    "file": "index.CyC-1HNi.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "pages/new-project-message.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "new-project-message.CLtnOHMi.js",
    "src": "pages/new-project-message.vue",
    "isDynamicEntry": true,
    "imports": [
      "_Breadcrumb.hG4ohCDR.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_Input.B69YdhzR.js",
      "_Divider.DWWHL2Rb.js",
      "_client-only.BiGpKdjH.js"
    ]
  },
  "pages/new-project-success.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "new-project-success.DkeWD-VL.js",
    "src": "pages/new-project-success.vue",
    "isDynamicEntry": true,
    "imports": [
      "_Breadcrumb.hG4ohCDR.js",
      "_BirthdayCard.BWQLCvui.js",
      "_client-only.BiGpKdjH.js",
      "_Input.B69YdhzR.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_ButtonGroup.ub5GH_tG.js",
      "_Divider.DWWHL2Rb.js"
    ],
    "css": [
      "new-project-success.ppEumpUl.css"
    ]
  },
  "new-project-success.ppEumpUl.css": {
    "file": "new-project-success.ppEumpUl.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "pages/new-project-template.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "new-project-template.R6z9weFI.js",
    "src": "pages/new-project-template.vue",
    "isDynamicEntry": true,
    "imports": [
      "_Breadcrumb.hG4ohCDR.js",
      "_client-only.BiGpKdjH.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "pages/project/[friendly-Id]/[surname].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "_surname_.CEMnhzBB.js",
    "src": "pages/project/[friendly-Id]/[surname].vue",
    "isDynamicEntry": true,
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_BirthdayCard.BWQLCvui.js",
      "_Divider.DWWHL2Rb.js"
    ]
  },
  "pages/project/[name].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "_name_.NvP2jr70.js",
    "src": "pages/project/[name].vue",
    "isDynamicEntry": true,
    "imports": [
      "_Breadcrumb.hG4ohCDR.js",
      "_BirthdayCard.BWQLCvui.js",
      "_client-only.BiGpKdjH.js",
      "_Input.B69YdhzR.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_ButtonGroup.ub5GH_tG.js",
      "_Divider.DWWHL2Rb.js"
    ]
  }
};

export { client_manifest as default };
//# sourceMappingURL=client.manifest.mjs.map
