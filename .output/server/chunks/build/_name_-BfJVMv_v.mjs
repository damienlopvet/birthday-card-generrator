import { c as useStorage, _ as __nuxt_component_0 } from './Breadcrumb-DTrAn1yb.mjs';
import { _ as __nuxt_component_1 } from './client-only-CF-yOZJ2.mjs';
import { _ as __nuxt_component_2, a as __nuxt_component_3 } from './Input-DFtLHW6F.mjs';
import { n as useRoute, d as __nuxt_component_4 } from './server.mjs';
import { ref, mergeProps, withCtx, unref, isRef, createVNode, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderList, ssrInterpolate } from 'vue/server-renderer';
import 'tailwind-merge';
import '../runtime.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'mongoose';
import 'node:fs';
import 'node:url';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import '@iconify/vue/dist/offline';
import '@iconify/vue';

const _sfc_main = {
  __name: "[name]",
  __ssrInlineRender: true,
  setup(__props) {
    useRoute();
    const guestName = ref("");
    const project = { project: { name: "", template: "", message: "", messageTitle: "", messageAuthor: "" } };
    useStorage("data", project);
    const guests = { guests: [] };
    const guestsData = useStorage("guests", guests);
    const links = [
      {
        label: "Home",
        icon: "i-heroicons-home",
        to: "/"
      },
      {
        label: "Template",
        icon: "i-heroicons-square-3-stack-3d"
      },
      {
        label: "Messsage",
        icon: "i-heroicons-chat-bubble-left-ellipsis-solid"
      },
      {
        label: "Pr\xE9noms",
        icon: "i-heroicons-users"
      }
    ];
    return (_ctx, _push, _parent, _attrs) => {
      const _component_UBreadcrumb = __nuxt_component_0;
      const _component_ClientOnly = __nuxt_component_1;
      const _component_UButtonGroup = __nuxt_component_2;
      const _component_UInput = __nuxt_component_3;
      const _component_UButton = __nuxt_component_4;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "space-y-6" }, _attrs))}>`);
      _push(ssrRenderComponent(_component_UBreadcrumb, { links }, null, _parent));
      _push(`<h1>Voici ta carte d&#39;anniversaire</h1><hr>`);
      _push(ssrRenderComponent(_component_ClientOnly, null, {}, _parent));
      _push(`<h2>ajoute maintenant les pr\xE9noms de tes invit\xE9s:</h2><form>`);
      _push(ssrRenderComponent(_component_UButtonGroup, {
        size: "xs",
        orientation: "horizontal"
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(_component_UInput, {
              required: "",
              maxlength: "30",
              modelValue: unref(guestName),
              "onUpdate:modelValue": ($event) => isRef(guestName) ? guestName.value = $event : null,
              class: "",
              placeholder: "G\xE9raldine,"
            }, null, _parent2, _scopeId));
            _push2(ssrRenderComponent(_component_UButton, {
              type: "submit",
              icon: "i-heroicons-arrow-small-right-20-solid",
              color: "gray"
            }, null, _parent2, _scopeId));
          } else {
            return [
              createVNode(_component_UInput, {
                required: "",
                maxlength: "30",
                modelValue: unref(guestName),
                "onUpdate:modelValue": ($event) => isRef(guestName) ? guestName.value = $event : null,
                class: "",
                placeholder: "G\xE9raldine,"
              }, null, 8, ["modelValue", "onUpdate:modelValue"]),
              createVNode(_component_UButton, {
                type: "submit",
                icon: "i-heroicons-arrow-small-right-20-solid",
                color: "gray"
              })
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</form><h2><ul><!--[-->`);
      ssrRenderList(unref(guestsData).guests, (guest) => {
        _push(`<li>${ssrInterpolate(guest)}</li>`);
      });
      _push(`<!--]--></ul></h2></div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/project/[name].vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=_name_-BfJVMv_v.mjs.map
