import { d as defineEventHandler, r as readBody } from '../../runtime.mjs';
import { p as project } from '../../_/project.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'mongoose';
import 'node:fs';
import 'node:url';

const index_put = defineEventHandler(async (event) => {
  const body = await readBody(event);
  console.log("body", body);
  if (!body.name || !body.template || !body.message || !body.messageAuthor) {
    event.node.res.statusCode = 400;
    return {
      code: "ERROR",
      message: "Invalid request"
    };
  }
  console.log("body.name", body.name);
  let existingProject = await project.exists({
    name: body.name
  });
  if (existingProject) {
    console.log("Project already exist", existingProject._id);
    const updatedProject = { ...body };
    console.log("updatingProject");
    await project.updateOne({
      id: existingProject._id,
      template: body.template,
      message: body.message,
      messageAuthor: body.messageAuthor
    });
    console.log("-------------------------------Project updated-------------------------------");
    return { project: { ...updatedProject } };
  } else {
    const newProject = new project(body);
    console.log("newProject", newProject);
    let res = await newProject.save();
    console.log("Project created", res);
    return { project: { ...res } };
  }
});

export { index_put as default };
//# sourceMappingURL=index.put.mjs.map
