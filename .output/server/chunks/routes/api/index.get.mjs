import { d as defineEventHandler } from '../../runtime.mjs';
import { p as project } from '../../_/project.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'mongoose';
import 'node:fs';
import 'node:url';

const index_get = defineEventHandler(
  async (event) => {
    console.log("event", event);
    try {
      const projects = await project.find();
      return projects;
    } catch (error) {
      console.dir(error);
      event.node.res.statusCode = 500;
      return {
        code: "ERROR",
        message: "Internal server error"
      };
    }
  }
);

export { index_get as default };
//# sourceMappingURL=index.get.mjs.map
