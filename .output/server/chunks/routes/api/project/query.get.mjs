import { d as defineEventHandler, g as getQuery } from '../../../runtime.mjs';
import { p as project } from '../../../_/project.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'mongoose';
import 'node:fs';
import 'node:url';

const query_get = defineEventHandler(
  async (event) => {
    const query = getQuery(event);
    const friendlyId = query.project;
    console.log("friendlyId", friendlyId);
    try {
      const res = await project.findOne({
        name: friendlyId
      });
      console.log("res", res);
      return { project: res };
    } catch (error) {
      console.dir(error);
      event.node.res.statusCode = 500;
      return {
        code: "ERROR",
        message: "Internal server error"
      };
    }
  }
);

export { query_get as default };
//# sourceMappingURL=query.get.mjs.map
