import mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    name: { type: String, unique: true },
    template: { type: String },
    message: { type: String },
    messageAuthor: { type: String }
  },
  { strict: true, strictQuery: false }
);
const project = mongoose.model("Project", schema, "project");

export { project as p };
//# sourceMappingURL=project.mjs.map
