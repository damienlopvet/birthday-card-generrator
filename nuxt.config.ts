// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['@vueuse/nuxt', '@nuxt/ui'], 
  runtimeConfig: {
    mongoUrl: process.env.MONGO_URL,
  },
  app: {
    pageTransition: { name: 'page', mode: 'out-in' }
  },
  nitro: {
    plugins: ["~/server/index.ts"],
  },

})