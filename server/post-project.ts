import project from "./dbModels/project";

interface IRequestBody {
    name: string;
    template: string;
    message: string;
}

export default defineEventHandler(async (event) => {
    const body: IRequestBody = await readBody<IRequestBody>(event)
    try {
        const newProject = new project(body);
        await newProject.save();
        return newProject;
    } catch (error) {
        console.dir(error);
        event.res.statusCode = 500;
        return {
            code: "ERROR",
            message: "Internal server error"
        };
    }
});
