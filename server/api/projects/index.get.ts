import project from "../../dbModels/project";

export default defineEventHandler(async (event) => {
    console.log("event", event);
    try {
        const projects = await project.find();
        return projects;   
    } catch (error) {
        console.dir(error);
        event.node.res.statusCode = 500;
        return {
            code: "ERROR",
            message: "Internal server error"
        }
    }
}
);