import project from "../../dbModels/project";

interface IRequestBody {
    name: string;
    template: string;
    message: string;
    messageTitle: string;
    messageAuthor: string;
}

export default defineEventHandler(async (event:any) => {
    const body: IRequestBody = await readBody<IRequestBody>(event)
    console.log("body", body);
    
    if (!body.name || !body.template || !body.message || !body.messageAuthor) {
        event.node.res.statusCode = 400;
        return {
            code: "ERROR",
            message: "Invalid request"
        };
    }


    console.log("body.name", body.name);
   let existingProject = await project.exists({
        name: body.name
    })
    if (existingProject) {
        console.log("Project already exist", existingProject._id);
            //if project already exist, update it with the body
            const updatedProject = { ...body };
            console.log("updatingProject");
            const res = await project.updateOne({
                id: existingProject._id,
                template: body.template,
                message: body.message,
                messageAuthor: body.messageAuthor
            });

            console.log("-------------------------------Project updated-------------------------------");
            return {project:{...updatedProject}}
        }
        else {
            const newProject = new project(body);
            console.log("newProject", newProject);
            let res = await newProject.save();
            console.log("Project created", res);
            return {project : {...res}}

        }
   
});
