import project from "../../dbModels/project";



export default defineEventHandler(async (event) => {
    const query = getQuery(event)

    const friendlyId = query.project
    console.log("friendlyId", friendlyId);
    try {
        const res = await project.findOne({
            name: friendlyId
        });
        console.log("res", res);
        return {project: res};
    } catch (error) {
        console.dir(error);
        event.node.res.statusCode = 500;
        return {
            code: "ERROR",
            message: "Internal server error"
        }
    }
}
)