import mongoose from "mongoose";

const schema = new mongoose.Schema(
    {
        name: { type: String, unique: true },
        template: { type: String },
        message: { type: String },
        messageAuthor: { type: String },
    },
    { strict: true, strictQuery: false }
);

export default mongoose.model("Project", schema, "project");;